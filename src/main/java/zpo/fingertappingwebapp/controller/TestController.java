package zpo.fingertappingwebapp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import zpo.fingertappingwebapp.persistance.entity.Test;
import zpo.fingertappingwebapp.persistance.entity.User;
import zpo.fingertappingwebapp.service.TestService;
import zpo.fingertappingwebapp.service.UsersService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class TestController {
    TestService testService;
    private UsersService usersService;

    @Autowired
    public TestController(TestService testService, UsersService usersService){
        this.testService = testService;
        this.usersService = usersService;
    }

    @GetMapping("/")
    public String index()  {
        return "index";
    }

    @GetMapping("/users")
    public String getUsers(Model model) throws ExecutionException, InterruptedException {
        List<User> users = usersService.getUsers();
        System.out.println(users.toString());
        model.addAttribute("users", users);
        return "users";
    }


    @PostMapping("/tests")
    public String getTests(@RequestParam String email, Model model) throws ExecutionException, InterruptedException {
        List<Test> tests = usersService.getTests(email);
        System.out.println(tests.toString());
        model.addAttribute("tests", tests);
        return "tests";
    }

    @PostMapping("/test")
    public String getTest(@RequestParam Test test, Model model) throws ExecutionException, InterruptedException {
        model.addAttribute("time", test.getTime().toArray());
        model.addAttribute("x", test.getAccelerometerData().getxAxis().toArray());
        model.addAttribute("y", test.getAccelerometerData().getyAxis().toArray());
        model.addAttribute("z", test.getAccelerometerData().getzAxis().toArray());
        return "test";
    }
}
