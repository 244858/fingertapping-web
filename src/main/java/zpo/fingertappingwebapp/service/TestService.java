package zpo.fingertappingwebapp.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;
import zpo.fingertappingwebapp.persistance.entity.Test;

import java.util.concurrent.ExecutionException;

@Service
public class TestService {

    public String addTest(Test test) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection("test").document(String.valueOf(test.getId())).set(test);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public Test getTest(Long id) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection("test").document(id.toString());
        ApiFuture<DocumentSnapshot> future = documentReference.get();

        DocumentSnapshot document = future.get();

        Test person = null;

        if(document.exists()) {
            person = document.toObject(Test.class);
            return person;
        }else {
            return null;
        }
    }

    public String updateTest(Test test) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection("test").document(String.valueOf(test.getId())).set(test);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public String deleteTest(Long id) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> writeResult = dbFirestore.collection("test").document(id.toString()).delete();
        return "Document with ID "+id+" has been deleted";
    }
}
