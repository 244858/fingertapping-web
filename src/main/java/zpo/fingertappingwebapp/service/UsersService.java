package zpo.fingertappingwebapp.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;
import zpo.fingertappingwebapp.persistance.entity.Test;
import zpo.fingertappingwebapp.persistance.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class UsersService {
    Firestore db = FirestoreClient.getFirestore();

    public List<User> getUsers() throws ExecutionException, InterruptedException {
        List<User> users = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("users").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for (DocumentSnapshot document : documents) {
            String username = (String) document.get("name");
            String email = (String) document.get("email");
            users.add(new User(username, email));
        }
        System.out.println(users.toString());
        return users;
    }

    public List<Test> getTests(String email) throws ExecutionException, InterruptedException {
        List<Test> tests = new ArrayList<>();
        DocumentReference docRef = db.collection("users").document(email);
        DocumentSnapshot doc = docRef.get().get();
        CollectionReference colRef = docRef.collection("tests");
        ApiFuture<QuerySnapshot> apiFuture = colRef.get();


        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for (DocumentSnapshot document : documents) {
            tests.add(document.toObject(Test.class));
        }
        return tests;
    }
}
