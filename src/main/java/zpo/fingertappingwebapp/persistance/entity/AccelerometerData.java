package zpo.fingertappingwebapp.persistance.entity;

import java.util.List;

public class AccelerometerData {
    private List<Float> xAxis;
    private List<Float> yAxis;
    private List<Float> zAxis;


    public AccelerometerData(){

    }
    public AccelerometerData(List<Float> xAxis, List<Float> yAxis, List<Float> zAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.zAxis = zAxis;
    }


    public List<Float> getxAxis() {
        return xAxis;
    }

    public void setxAxis(List<Float> xAxis) {
        this.xAxis = xAxis;
    }

    public List<Float> getyAxis() {
        return yAxis;
    }

    public void setyAxis(List<Float> yAxis) {
        this.yAxis = yAxis;
    }

    public List<Float> getzAxis() {
        return zAxis;
    }

    public void setzAxis(List<Float> zAxis) {
        this.zAxis = zAxis;
    }

    @Override
    public String toString() {
        return "AccelerometerData{" +
                "xAxis=" + xAxis.toString() +
                ", yAxis=" + yAxis.toString() +
                ", zAxis=" + zAxis.toString() +
                '}';
    }
}
