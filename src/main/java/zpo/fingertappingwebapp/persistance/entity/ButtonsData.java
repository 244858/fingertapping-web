package zpo.fingertappingwebapp.persistance.entity;

import java.util.List;

public class ButtonsData {
    private List<Float> leftButtonClicks;
    private List<Float> rightButtonClicks;

    public ButtonsData(){

    }

    public ButtonsData(List<Float> leftButtonClicks, List<Float> rightButtonClicks) {
        this.leftButtonClicks = leftButtonClicks;
        this.rightButtonClicks = rightButtonClicks;
    }

    public List<Float> getLeftButtonClicks() {
        return leftButtonClicks;
    }

    public void setLeftButtonClicks(List<Float> leftButtonClicks) {
        this.leftButtonClicks = leftButtonClicks;
    }

    public List<Float> getRightButtonClicks() {
        return rightButtonClicks;
    }

    public void setRightButtonClicks(List<Float> rightButtonClicks) {
        this.rightButtonClicks = rightButtonClicks;
    }

    @Override
    public String toString() {
        return "ButtonsData{" +
                "leftButtonClicks=" + leftButtonClicks.toString() +
                ", rightButtonClicks=" + rightButtonClicks.toString() +
                '}';
    }
}
