package zpo.fingertappingwebapp.persistance.entity;

import java.util.List;

public class Test {
    private int id;
    private List<Float> time;
    private AccelerometerData accelerometerData;
    private ButtonsData buttonsData;

    public Test(){

    }
    public Test(int id, List<Float> time, AccelerometerData accelerometerData, ButtonsData buttonsData) {
        this.id = id;
        this.time = time;
        this.accelerometerData = accelerometerData;
        this.buttonsData = buttonsData;
    }

    public Test(List<Float> time, AccelerometerData accelerometerData) {
        this.time = time;
        this.accelerometerData = accelerometerData;
    }


    public ButtonsData getButtonsData() {
        return buttonsData;
    }

    public void setButtonsData(ButtonsData buttonsData) {
        this.buttonsData = buttonsData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Float> getTime() {
        return time;
    }

    public void setTime(List<Float> time) {
        this.time = time;
    }

    public AccelerometerData getAccelerometerData() {
        return accelerometerData;
    }

    public void setAccelerometerData(AccelerometerData accelerometerData) {
        this.accelerometerData = accelerometerData;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", time=" + time.toString() +
                ", accelerometerData=" + accelerometerData.toString() +
                ", buttonsData=" + buttonsData.toString() +
                '}';
    }
}
