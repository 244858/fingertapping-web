package zpo.fingertappingwebapp.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zpo.fingertappingwebapp.persistance.entity.Test;

@Repository
public interface TestRepository extends CrudRepository<Test, Long> {
}
