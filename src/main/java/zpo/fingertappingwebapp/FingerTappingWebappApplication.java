package zpo.fingertappingwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FingerTappingWebappApplication {

    public static void main(String[] args) {
        SpringApplication.run(FingerTappingWebappApplication.class, args);
    }

}
